package main

import (
	"path/filepath"
	"os"
	"io/ioutil"
	"path"
	"debug/elf"
	"log"
	"os/exec"
	"bufio"
	"runtime"
	"strings"
	"fmt"
	"sync"
)

var logger = log.New(os.Stderr, "", 0)
var out = log.New(os.Stdout, "", 0)

var visited = make(map[string]bool)
var found = make(map[string]bool)

var defaultLdso = ""
var quiet = false

func init()  {
	switch runtime.GOARCH {
	case "amd64":
		defaultLdso = "/lib64/ld-linux-x86-64.so.2"
		break
	case "arm64":
		defaultLdso = "/lib/ld-linux-aarch64.so.1"
		break
	default:
		defaultLdso = "/lib/ld-linux.so.2"
	}
}

func handle(p string, strict bool) (err error) {
	p, err = filepath.Abs(p)
	if err != nil {
		return err
	}
	if visited[p] == true {
		//already handled this one
		return nil
	}
	visited[p] = true

	if isDir(p) {
		entries, err := ioutil.ReadDir(p)
		if err != nil {
			return err
		}

		for _, element := range entries {
			for err = handle(path.Join(p, element.Name()), false); err != nil; {
				return err
			}
		}
	} else if isFile(p) {
		for err = handleFile(p); err != nil; {
			return err
		}
	} else {
		if strict {
			//path does not exist, raise error in strict mode
			return fmt.Errorf("path '%s' does not exist or is invalid", p)
		}
	}

	return nil
}

func handleFile(p string) (err error) {
	f, err := os.Open(p)
	if err != nil {
		return err
	}
	defer f.Close()

	elfFile, err := elf.NewFile(f)
	if err != nil {
		if !quiet {
			logger.Printf("skipping '%s', not an elf file\n", p)
		}
		return nil //skip file
	}

	//could be an elf file, let's check the header
	// Read and decode ELF identifier
	var ident [16]uint8
	_, err = f.ReadAt(ident[0:], 0)
	if err != nil {
		if !quiet {
			logger.Printf("skipping '%s', not an elf file\n", p)
		}
		return nil //skip file
	}

	if ident[0] != '\x7f' || ident[1] != 'E' || ident[2] != 'L' || ident[3] != 'F' {
		if !quiet {
			logger.Printf("skipping '%s', not an elf file\n", p)
		}
		return nil //skip file
	}

	if elfFile.Type != elf.ET_DYN && elfFile.Type != elf.ET_EXEC {
		if !quiet {
			logger.Printf("skipping '%s', wrong elf type\n", p)
		}
		return nil //skip file
	}

	//ok, this is an elf file we understand
	found[p] = true

	interp := elfFile.Section(".interp")
	if interp != nil && interp.Size > 1 {
		v, err := readString(interp)
		if err == nil && len(v) > 0 && path.IsAbs(v) {
			if isFile(v) {
				//great, found the interpreter
				found[v] = true
				return findDeps(p, v)
			}
		}
	}

	//no interpreter found for this binary, we need to use the default ldso
	return findDeps(p, defaultLdso)
}

func findDeps(elfPath string, ldso string) (err error) {
	cmd := exec.Command(ldso, elfPath)
	cmd.Env = append(os.Environ(),
		"LD_TRACE_LOADED_OBJECTS=1",
	)
	outPipe, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	wg.Add(1)

	var deps []string
	go func() {
		defer outPipe.Close()
		defer wg.Done()
		scanner := bufio.NewScanner(outPipe)
		for scanner.Scan() {
			line := scanner.Text()
			line = strings.TrimSpace(line)
			if strings.Contains(line, "=>") {
				parts := strings.Split(line, "=>")
				if len(parts) == 2 {
					name := strings.TrimSpace(parts[0])
					value := strings.TrimSpace(parts[1])
					if len(name) > 0 && len(value) > 0 {
						if strings.HasPrefix(value, "/") {
							//looks like its a file
							if strings.Contains(value, "(0x") {
								//remove address
								parts = strings.Split(value, "(0x")
								if len(parts) != 2 {
									logger.Fatalf("failed to parse string '%s'", value)
								}
								value = parts[0]
							}
							value = strings.TrimSpace(value)
							if len(value) > 0 {
								deps = append(deps, value)
							}
						}
					}
				}
			}
		}
	}()

	cmd.Stderr = os.Stderr
	for err = cmd.Start(); err != nil; {
		return err
	}

	for err = cmd.Wait(); err != nil; {
		return err
	}

	wg.Wait() //make sure the above goroutine is done

	for _, dep := range deps {
		if isFile(dep) {
			err = handle(dep, false)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func readString(section *elf.Section) (value string, err error) {
	if section.Size == 0 {
		return "", nil
	}
	data, err := section.Data()
	if err != nil {
		return "", err
	}
	return string(data[:section.Size - 1]), nil
}

func isDir(path string) (bool) {
	info, err := os.Stat(path)
	if err != nil {
		return false
	}
	return info.IsDir()
}

func isFile(path string) (bool) {
	info, err := os.Stat(path)
	if err != nil {
		return false
	}
	return info.Mode().IsRegular()
}

func usage(cmdName string)  {
	logger.Printf("Usage: %s (-q|--quiet) FILES...\n", cmdName)
	os.Exit(1)
}

func main()  {
	envLdso := os.Getenv("DEFAULT_LDSO")
	if len(envLdso) > 0 {
		defaultLdso = envLdso
	}
	if !isFile(defaultLdso) {
		logger.Fatalf("default ldso '%s' not found, please point env variable DEFAULT_LDSO to your ldso", defaultLdso)
	}
	var files []string
	cmdName := os.Args[0]

	for _, arg := range os.Args[1:] {
		switch arg {
		case "-h": usage(cmdName)
		case "--help": usage(cmdName)
		case "-q": quiet = true; break
		case "--quiet": quiet = true; break
		default:
			files = append(files, arg)
		}
	}

	if len(files) == 0 {
		logger.Println("no files specified")
		usage(cmdName)
	}

	for _, file := range files {
		err := handle(file, true)
		if err != nil {
			logger.Fatalf("error: %s", err)
		}
	}

	for k, _ := range found {
		out.Println(k)
	}
}
